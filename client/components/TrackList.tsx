import React from 'react'
import {ITrack} from '../types/track'
import {Box, Grid} from '@material-ui/core'
import Track from './Track';

interface IProps {
  tracks: ITrack[]
}

const TrackList: React.FC<IProps> = ({ tracks }): JSX.Element => {
  return (
    <Grid container alignItems="center" direction="column">
      <Box p={3}>
        {tracks.map((track: ITrack) => <Track key={track._id} track={track} />)}
      </Box>
    </Grid>
  );
};

export default TrackList