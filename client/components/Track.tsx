import React from 'react';
import {ITrack} from '../types/track'
import {Card, Grid, IconButton} from '@material-ui/core'
import styles from '../styles/Track.module.scss'
import {Delete, Pause, PlayArrow} from '@material-ui/icons'

interface IProps {
  track: ITrack;
  active?: boolean;
}

const Track: React.FC<IProps> = ({ track, active= true }): JSX.Element => {
  const { name, artist, text, picture: src, audio } = track

  return (
    <Card className={styles.track}>
      <IconButton>
        {active ? <Pause /> : <PlayArrow />}
      </IconButton>
      <img width={60} height={60} src={src} alt="Обложка трека" className={styles.img} />
      <Grid container direction="column" style={{  width: '200px', margin: '0 20px' }}>
        <p>{name}</p>
        <p style={{ fontSize: 12, color: 'gray' }}>{artist}</p>
      </Grid>
      {active && <div>02:43 / 03:49</div>}
      <IconButton style={{ marginLeft: 'auto' }}>
        <Delete />
      </IconButton>
    </Card>
  );
};

export default Track