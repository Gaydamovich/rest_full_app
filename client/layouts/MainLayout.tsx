import React from 'react'
import NavBar from '../components/NavBar';
import {Container} from '@material-ui/core';

const MainLayout: React.FC = ({ children }) => {
  return (
    <>
      <NavBar />
      <Container style={{ marginTop: '100px' }}>
        {children}
      </Container>
    </>
  )
}

export default MainLayout