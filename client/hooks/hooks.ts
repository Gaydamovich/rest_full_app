import { useState } from 'react'

export function useInput(initial) {
  const [value, setValue] = useState(initial)
  const onChange = ({ target }) => setValue(target.value)
  return { value, onChange }
}