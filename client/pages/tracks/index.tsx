import React from 'react'
import MainLayout from '../../layouts/MainLayout'
import {Box, Button, Card, Grid, TextField} from '@material-ui/core'
import {useRouter} from 'next/router'
import {ITrack} from '../../types/track'
import TrackList from "../../components/TrackList";

const tracks: ITrack[] = [
  {
    _id: 'at134y52qgDSGA',
    name: 'Run',
    artist: 'Bilan',
    text: 'No text...',
    listens: 10,
    picture: 'http://localhost:4200/image/b31f8ee7-7020-470b-8834-88d97a5ff7e3.jpg',
    audio: 'http://localhost:4200/audio/3df7ebf5-6e3c-43f0-8e26-c999c7707821.mp3',
    comments: [],
  },
  {
    _id: 'at134y52qgDSGA',
    name: 'Snow',
    artist: 'Dan Balan',
    text: 'No text...',
    listens: 23214,
    picture: 'http://localhost:4200/image/b31f8ee7-7020-470b-8834-88d97a5ff7e3.jpg',
    audio: 'http://localhost:4200/audio/3df7ebf5-6e3c-43f0-8e26-c999c7707821.mp3',
    comments: [],
  },
]

const Index = () => {
  const router = useRouter()

  return (
    <MainLayout>
      <Grid container justify="center">
        <Card style={{ width: '900px' }}>
          <Box p={3}>
            <Grid container justify="space-between" alignItems="center">
              <h1 style={{ color: 'GrayText' }}>Список треков</h1>
              <Button
                style={{ height: '50px', width: '150px' }}
                variant="contained"
                color="primary"
                onClick={() => router.push('/tracks/create')}
              >
                Загрузить
              </Button>
            </Grid>
          </Box>

          <TrackList tracks={tracks} />
        </Card>

      </Grid>


    </MainLayout>
  )
}

export default Index