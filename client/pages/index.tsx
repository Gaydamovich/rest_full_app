import React from 'react'
import NavBar from '../components/NavBar'
import MainLayout from "../layouts/MainLayout";
import {
  CircularProgress, LinearProgress,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField, Typography, TypographyVariant
} from "@material-ui/core";
import {useInput} from "../hooks/hooks";

const Index = () => {
  const input = useInput('');

  return (
    <>

      <MainLayout>
        <div className="center">
          <h1>Добро пожаловать</h1>

          <h3>Здесь собраны лучшие треки</h3>
        </div>
      </MainLayout>
      <style jsx>
        {`  
          .center {
            margin-top: 100px;
            display: flex;
            flex-direction: column;
            align-items: center;
          }
        `}
      </style>
    </>
  )
}

export default Index