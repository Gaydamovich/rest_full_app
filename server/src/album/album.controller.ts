import {Controller, Get} from "@nestjs/common";
import {AlbumService} from "./album.service";

@Controller('/albums')
export class AlbumController {
  constructor(private albumService: AlbumService) {}

  @Get('/all')
  getAll () {
    return this.albumService.getAll()
  }
}