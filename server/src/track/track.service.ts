import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model, ObjectId} from "mongoose";
import {Track, TrackDocument} from "./schemas/track.schema";
import {Comment, CommentDocument} from "./schemas/comment.schema";
import {CreateTrackDto} from "./dto/create-track.dto";
import {CreateCommentDto} from "./dto/create-comment.dto";
import {FileService, FileType} from "../file/file.service";

// noinspection ES6RedundantAwait
@Injectable()
export class TrackService {
  constructor(
    @InjectModel(Track.name) private trackModel: Model<TrackDocument>,
    @InjectModel(Comment.name) private commentModel: Model<CommentDocument>,
    private fileService: FileService,
  ) {}

  async create (dto: CreateTrackDto, picture, audio): Promise<Track> {
    const pictureFile = this.fileService.createFile(FileType.IMAGE, picture)
    const audioFile = this.fileService.createFile(FileType.AUDIO, audio)
    const track = await this.trackModel.create({ ...dto, listens: 0, audio: audioFile, picture: pictureFile })

    return track
  }

  async remove (id: ObjectId): Promise<Track> {
    return await this.trackModel.findByIdAndRemove(id)
  }

  async getOne (id: ObjectId): Promise<Track> {
    return await this.trackModel.findById(id).populate('comments')
  }

  async getAll (count = '10', offset = '0'): Promise<Track[]> {
    return await this.trackModel
      .find()
      .skip(Number(offset))
      .limit(Number(count))
  }

  async search (query: string): Promise<Track[]> {
    const tracks = this.trackModel.find({
      name: { $regex: new RegExp(query) }
    })

    return tracks
  }

  async addComment(dto: CreateCommentDto): Promise<Comment> {
    const track = await this.trackModel.findById(dto.track_id)
    const comment = await this.commentModel.create({ ...dto })
    track.comments.push(comment._id)
    await track.save()

    return comment
  }

  async listen(id: ObjectId) {
    const track = await this.trackModel.findById(id)
    track.listens++
    track.save()
  }
}